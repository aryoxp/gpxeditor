class CtxMenu {

  constructor(map, menus = [], handler) {

    this.map = map;
    // this.listeners = new Map()
    this.menu = new Menu()
    menus.forEach(m => {
      let nm = (m.type == "separator") ? m : Object.assign(m, {
        click: handler
      })
      this.menu.append(new MenuItem(nm))
    });

  }

  show() {
    this.menu.popup({
      window: remote.getCurrentWindow()
    })
  }

  static markerInstance(map, handler) {
    return new CtxMenu(map, [
      {
        id: "copy",
        label: 'Copy'
      },
      {
        id: "paste",
        label: 'Paste'
      },
      {
        type: 'separator'
      },
      {
        id: "all-following",
        label: 'Select trailing path'
      },
      {
        id: "all-following-working",
        label: 'Select trailing path on working segment'
      },
      {
        type: 'separator'
      },
      {
        id: "adjust-time",
        label: 'Adjust time'
      },
      {
        type: 'separator'
      },
      {
        id: "delete",
        label: 'Delete selected markers'
      },
    ], handler)
  }

  static segmentInstance(map, handler) {
    return new CtxMenu(map, [
      {
        id: "split",
        label: 'Split segment'
      },
    ], handler)
  }

  static mapInstance(map, handler) {
    return new CtxMenu(map, [
      {
        id: "add-point",
        label: 'Add point here'
      },
    ], handler)
  }

}