class GPXData {

  constructor() {
    this.path = new Map();
    this.items = new vis.DataSet();

    this.segmentsMap = new Map();

    this.nid = 0 // new path id;
  }

  get pathArray() {
    return Array.from(this.path.values())
  }

  get length() {
    return this.path.size
  }

  get size() {
    return this.path.size
  }

  get first() {
    return this.path.values().next().value
  }

  get last() {
    let node = this.first
    if(!node) return null
    do {
      if (node.next == null) return node
    } while (node = node.next)
  }

  get head() {
    return this.first
  }

  get tail() {
    return this.last
  }

  get segments() {
    return this.segmentsMap
  }

  getSegment(lat, lng) {

    let node = this.first
    let minDistance = Infinity
    let minId = null;
    do {
      if (!node.next) break;
      let p = {
        lat: lat,
        lng: lng,
      }
      let p1 = node
      let p2 = node.next
      let d = Haversine.perpendicularDistance(p.lng, p.lat, p1.lng, p1.lat, p2.lng, p2.lat)
      if (d < minDistance) {
        minDistance = d;
        minId = node.id
        if (d < 0.000001) break;
      }
    } while (node = node.next)

    for (const [key, s] of this.segments) {
      if (s.points.has(minId)) {
        return s;
      }
    }

    return null;
  }

  getSegmentWithPoint(pointId) {
    for (const [key, s] of this.segments) {
      if (s.points.has(pointId)) return s;
    }
    return null;
  }

  getDataSet() {
    return this.items;
  }

  getPath() {
    return this.path
  }

  parseGpxString(stringData) {
    const parser = new DOMParser();
    const dom = parser.parseFromString(stringData, "application/xml");
    let itemArray = []
    let trkpts = Array.from(dom.getElementsByTagName('trkpt')); // console.log(trkpts)
    let id = 0;
    let pputc = null;

    let prev = null;

    if (this.items.length) this.items.clear()
    if (this.path.size) this.path.clear()
    trkpts.forEach(tp => {
      let ele = parseFloat(tp.getElementsByTagName('ele')[0].textContent)
      if(ele == -10) console.warn(ele)
      ele = ele > -10 ? ele : undefined
      // Data for Google Maps
      let p = {
        id: id,
        lat: parseFloat(tp.getAttribute("lat")),
        lng: parseFloat(tp.getAttribute("lon")),
        ele: ele,
        speed: parseFloat(tp.getElementsByTagName('speed')[0].textContent),
        time: tp.getElementsByTagName('time')[0].textContent,
        utc: Date.parse(tp.getElementsByTagName('time')[0].textContent) / 1000,
        utcd: pputc != null ?
          Date.parse(tp.getElementsByTagName('time')[0].textContent) / 1000 - pputc : 0,
        prev: null,
        next: null
      };
      this.path.set(id, p);

      // Data for Vis
      let item = {
        id: id,
        type: 'point',
        start: p.time,
        content: "", //'S' + Math.floor(id/app.settings.segmentSize),
        point: p
      };
      // this.items.add(item);
      itemArray.push(item)

      pputc = p.utc

      // Construct Dual Linked List
      if (prev != null) {
        prev.next = p;
        p.prev = prev;
      }
      prev = p;

      id++;
    });
    this.items.clear()
    this.items.add(itemArray)
  }

  get nextNid() {
    return this.nid++
  }

  add(lat, lng, time, point = null) {

    let p = {
      id: "N" + this.nextNid,
      lat: lat,
      lng: lng,
      ele: undefined,
      speed: 0,
      time: time,
      utc: Date.parse(time) / 1000,
      utcd: null,
      prev: null,
      next: null
    };
    this.path.set(p.id, p);

    // Data for Vis
    let item = {
      id: p.id,
      type: 'point',
      start: p.time,
      content: "", //'S' + Math.floor(id/app.settings.segmentSize),
      point: p
    };
    this.items.add(item);

    // Modify Dual Linked List chain
    if(point) {
      p.next = point.next;
      p.prev = point
      if (p.next) p.next.prev = p
      point.next = p
    }

    // Update utc and utcd
    p.utcd = p.prev ? p.utc - p.prev.utc : 0
    if(p.next) p.next.utcd = p.next.utc - p.utc

    return p;
  }

  insertAt(point, lat, lng, ele, speed, time, utc, id = null) {
    let p = {
      id: "N" + this.nextNid,
      lat: lat,
      lng: lng,
      ele: ele,
      speed: speed,
      time: time,
      utc: Date.parse(time) / 1000,
      utcd: utc,
      prev: null,
      next: null
    };
    this.path.set(p.id, p);

    // Data for Vis
    let item = {
      id: p.id,
      type: 'point',
      start: p.time,
      content: "", //'S' + Math.floor(id/app.settings.segmentSize),
      point: p
    };
    this.items.add(item);

    // Modify Dual Linked List chain
    p.next = point.next;
    p.prev = point
    if (p.next) p.next.prev = p
    point.next = p

    // Update utc and utcd
    p.utcd = p.prev ? p.utc - p.prev.utc : 0
    if (p.next) p.next.utcd = p.next.utc - p.utc

    return p;
  }

  delete(ids, callback) {
    ids.forEach(id => {
      let p = this.path.get(id)
      if (typeof callback == "function") callback(p)
      let pp = p.prev
      let np = p.next
      if (pp) pp.next = np
      if (np) {
        np.prev = pp
        np.utcd = np.utc - pp.utc
      }
      this.items.remove(id)
      this.path.delete(id)
    })
  }

  segmentize() {
    let node = this.first
    this.segmentsMap.clear()
    let sid = 0;
    let segment = new Segment(sid)
    if (!node) return;
    do {
      if (segment.points.size == 0) segment.minId = node.id
      segment.points.set(node.id, node)
      if (segment.points.size == Config.segmentSize || node.next == null) {
        segment.maxId = node.id
        this.segmentsMap.set(sid, segment)
        sid++
        segment = new Segment(sid);
      }
    } while (node = node.next)
    return this.segmentsMap;
  }

  walk(from = null, until = null, copy = false) {
    let node = from ? this.path.get(from) : this.first
    let walkPath = []
    if (!node) return;
    do {
      if (!copy) walkPath.push(node)
      else walkPath.push({
        ele: node.ele,
        id: node.id,
        lat: node.lat,
        lng: node.lng,
        next: null,
        prev: null,
        speed: node.speed,
        time: node.time,
        utc: node.utc,
        utcd: null
      })
      if (until == node.id) break;
    } while (node = node.next)
    return walkPath;
  }

  reverseWalk(from = null, until = null) {
    let node = from ? this.path.get(from) : this.last
    let walkPath = []
    if (!node) return;
    do {
      walkPath.push(node)
      if (until == node.id) break;
    } while (node = node.prev)
    return walkPath;
  }

  static getIds(path) {
    let ids = []
    path.forEach(p => ids.push(p.id));
    return ids;
  }

  static findHeadAndTail(markers) {
    let markerMap = new Map()
    markers.forEach(i => markerMap.set(i.point.id, i))
    let head = null;
    let tail = null;
    Array.from(markerMap.values()).forEach(m => {
      console.log(m)
      if (m.point.prev == null) head = m.point
      if (m.point.next == null) tail = m.point
      if (!head && m.point.prev != null) {
        if (!markerMap.has(m.point.prev.id)) head = m.point
      }
      if (!tail && m.point.next != null) {
        if (!markerMap.has(m.point.next.id)) tail = m.point
      }
      if (!head && !tail) return {
        head: head,
        tail: tail
      }
    })
    return {
      head: head,
      tail: tail
    }
  }

}