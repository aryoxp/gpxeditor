class Segment {

  constructor(sid) {
    this.id = sid
    this.points = new Map()
    this.maxId = null
    this.minId = null

    this.markers = new Map()
    this.selectedMarkers = []
  }

  clearSelectedMarkers() {
    for (let i = this.selectedMarkers.length - 1; this.selectedMarkers.length; i--) {
      let p = this.selectedMarkers[i].point
      let icon = p.utcd >= (2 * Config.tickLength) ? 
          "long-delay-marker" : "unselected-marker"
        icon = !p.ele ? "zero-elevation-marker" : icon
      this.selectedMarkers[i].setIcon(M.icon(icon))
      this.selectedMarkers.splice(i, 1)
    }
  }

  delete(ids) {
    // delete the markers
    this.markers.forEach((m, k) => {
      m.setMap(null)
      if (ids.includes(m.point.id)) this.markers.delete(k)
    })

    // delete the points
    ids.forEach(i => this.points.delete(i))
  }

  deleteMarkers() {
    for (const [k, m] of this.markers) {
      m.setMap(null)
    }
    this.markers.clear()
  }

  markMarkers(bounds) {
    this.clearSelectedMarkers()
    for (const [key, m] of this.markers) {
      if (bounds.contains({
          lat: m.point.lat,
          lng: m.point.lng
        })) {
        m.setIcon(M.icon("selected-marker"))
        this.selectedMarkers.push(m);
      }
    }
  }

  get path() {
    let path = []
    let node = this.points.get(this.minId)
    if (!node) return path
    do {
      path.push(new G.LatLng(node.lat, node.lng))
      if (node.id == this.maxId) break;
    } while (node = node.next)
    return path
  }

  get selectedMarkerIds() {
    let ids = []
    for (let m of this.selectedMarkers) {
      ids.push(m.point.id)
    }
    return ids;
  }

  get segmentTimeline() {
    return {
      id: "S" + this.id,
      content: "Segment #" + this.id,
      start: this.points.get(this.minId).time,
      end: this.points.get(this.maxId).time,
      type: "background",
    }
  }

  segAt(lat, lng) {
    let markerArray = Array.from(this.markers.values());
    let minDistance = Infinity
    let seg = null;
    for (let i = 0; i < markerArray.length - 1; i++) {
      let d = Haversine.perpendicularDistance(
        lng,
        lat,
        markerArray[i].point.lng,
        markerArray[i].point.lat,
        markerArray[i + 1].point.lng,
        markerArray[i + 1].point.lat
      )
      if (d < minDistance) {
        seg = [markerArray[i], markerArray[i + 1]]
        minDistance = d
      }
    }
    return seg;
  }

  getMarkerAt(pointId) {
    for (const [key, m] of this.markers) {
      if (m.point.id == pointId) return m;
    }
    return null;
  }

  update() {
    let node = this.points.get(this.minId)
    for (const [k, m] of this.markers) m.setMap(null)
    this.points.clear()
    this.markers.clear()
    do {
      if (!node) break;
      this.points.set(node.id, node);
      if (node.id == this.maxId) break;
    } while (node = node.next)
  }

}