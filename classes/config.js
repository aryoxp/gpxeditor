class Config {

  static segmentSize = 50
  static lineSymbol = {
    path: "M 0,-2 0,2",
    strokeOpacity: 1,
    fillColor: "red",
    scale: 2,
  }
  static tickLength = 5 // seconds per tick

  static get Timeline() {
    let t = new Date()
    t.setSeconds(t.getSeconds() + 45)
    return {
      horizontalScroll: true,
      zoomable: false,
      min: new Date(),
      max: t
    }
  }
}