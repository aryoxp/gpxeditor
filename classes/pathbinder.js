class PathBinder {
  constructor(path) {
    this.polyPath = path;
  }
  get(key) {
    if (!isNaN(parseInt(key))) {
      return this.polyPath.getAt(parseInt(key));
    } else {
      this.polyPath.get(key);
    }
  }
  set(key, val) {
    if (!isNaN(parseInt(key))) {
      this.polyPath.setAt(parseInt(key), val);
    } else {
      this.polyPath.set(key, val);
    }
  }
}

var G = google.maps;
PathBinder.prototype = new G.MVCObject();
