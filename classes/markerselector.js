class MarkerSelector {

  constructor(map) {
    this.map = map;

    this.shift = false;
    this.bounds = new G.LatLngBounds();

    this.handleEvent();
    this.handleMagnetEvent();
    this.listeners = new Map();
  }

  handleEvent() {
    let app = this;
    $(document).on('keydown', (e) => {
      if (e.shiftKey) {
        this.shift = true;
        app.map.setOptions({
          draggable: false
        });
        // console.log("shift")
      }
    })

    $(document).on('keyup', (e) => {
      // console.log(e.shiftKey);
      if (this.shift && !e.shiftKey) {
        this.shift = false;
        app.map.setOptions({
          draggable: true
        });

        // if shift is released but left click yet released
        // clear gripBoundingBox
        if (this.gripBoundingBox) this.gripBoundingBox.setMap(null);
        this.firstPoint = null;
        this.gripBoundingBox = null;
        // console.log("shift-false")
      }
    })

    G.event.addListener(app.map, 'mousedown', (e) => { // console.log(e)
      if (this.shift) {
        this.firstPoint = new G.LatLng(e.latLng.lat(), e.latLng.lng());
        this.down = true;
      }
      // if(e.vb.button == 2) e.stop()
    })
    G.event.addListener(app.map, 'mousemove', (e) => {
      if (this.shift && this.down) {
        this.drag = true;
        this.bounds = new G.LatLngBounds();
        this.bounds.extend(this.firstPoint)
        this.bounds.extend(new G.LatLng(e.latLng.lat(), e.latLng.lng()))
        if (this.gripBoundingBox) this.gripBoundingBox.setMap(null);
        this.gripBoundingBox = new google.maps.Rectangle({
          map: app.map,
          bounds: this.bounds,
          strokeColor: "#FF0000",
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: "#FF0000",
          fillOpacity: 0.35,
          clickable: false
        });
      }
    })
    G.event.addListener(app.map, 'mouseup', (e) => {
      // console.log(this.shift, this.drag)
      this.down = false;
      if (this.shift && this.drag) {
        this.drag = false
        this.bounds = null;
        this.listeners.forEach((callback) => {
          callback("bounding-box", {
            ne: {
              lat: this.gripBoundingBox.getBounds().getNorthEast().lat(),
              lng: this.gripBoundingBox.getBounds().getNorthEast().lng(),
            },
            sw: {
              lat: this.gripBoundingBox.getBounds().getSouthWest().lat(),
              lng: this.gripBoundingBox.getBounds().getSouthWest().lng(),
            }
          });
        });

        if (this.gripBoundingBox) this.gripBoundingBox.setMap(null);
        this.firstPoint = null;
        this.gripBoundingBox = null;

      }
    })

  }

  handleMagnetEvent() {
    let app = this;
    $(document).on('keydown', (e) => { // console.log(e)
      if (e.ctrlKey) {
        app.ctrl = true
        app.magnetListener = G.event.addListener(app.map, 'mousemove', (e) => {
          app.magnetPosition = e.latLng;
        })
      }
    })

    $(document).on('keyup', (e) => { // console.log(e)
      if (this.ctrl && !e.ctrlKey) {
        this.ctrl = false;
        G.event.removeListener(app.magnetListener)
      }
      if(this.ctrl && e.code == "KeyS") {
        this.listeners.forEach((callback) => {
          if(e.shiftKey) callback("ctrl-shift-s", app.magnetPosition);
          else callback("ctrl-s", app.magnetPosition);
        });
      }
    })
  }

  addListener(m) {
    if (typeof m == "function") this.listeners.set(m, m);
  }

  removeListener(m) {
    this.listeners.delete(m);
  }

}

class M {
  static icon (p) { // console.log(p)
    if (typeof p == "string") {
      switch (p) {
        case 'long-delay':
          return {
            path: "M-2,0a2,2 0 1,0 4,0a2,2 0 1,0 -4,0",
              fillColor: "white",
              fillOpacity: 1,
              scale: 3,
              strokeColor: "#4287f5",
              strokeWeight: 4,
              strokeOpacity: 1,
              labelOrigin: new google.maps.Point(0, -6)
          }
          break;
        case 'zero-elevation':
          return {
            path: "M-2,0a2,2 0 1,0 4,0a2,2 0 1,0 -4,0",
              fillColor: "white",
              fillOpacity: 1,
              scale: 3,
              strokeColor: "orange", // "#28a745",
              strokeWeight: 4,
              strokeOpacity: 1,
              labelOrigin: new google.maps.Point(0, -6)
          }
          break;
        case 'cluster-long-delay':
          return {
            path: "M-2,0a2,2 0 1,0 4,0a2,2 0 1,0 -4,0",
              fillColor: "#4287f5",
              fillOpacity: 1,
              scale: 6,
              strokeColor: "#4287f5", // "#28a745",
              strokeWeight: 10,
              strokeOpacity: 0.5,
              // labelOrigin: new google.maps.Point(0, -6)
          }
          break;
        case 'cluster-zero-elevation':
          return {
            path: "M-2,0a2,2 0 1,0 4,0a2,2 0 1,0 -4,0",
              fillColor: "orange",
              fillOpacity: 1,
              scale: 6,
              strokeColor: "orange", // "#28a745",
              strokeWeight: 10,
              strokeOpacity: 0.5,
              // labelOrigin: new google.maps.Point(0, -6)
          }
          break;
        case 'finish':
          return {
            url: "finish.svg",
              anchor: new google.maps.Point(10, 10),
              scaledSize: new google.maps.Size(20, 20)
          }
          break;
        case 'start':
          return {
            url: "start.svg",
              anchor: new google.maps.Point(15, 30),
              scaledSize: new google.maps.Size(30, 30)
          }
          break;
        case 'selected-marker':
          return {
            path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
            fillColor: "red",
            fillOpacity: 1,
            scale: 0.25,
            strokeColor: 'red',
            strokeWeight: 1,
            strokeOpacity: 1
          }
          break;
        case 'unselected-marker':
          return {
            path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
            fillColor: "white",
            fillOpacity: 1,
            scale: 0.25,
            strokeColor: 'red',
            strokeWeight: 2,
            strokeOpacity: 1
          }
          break;
        case 'long-delay-marker':
          return {
            path: "M-2,0a2,2 0 1,0 4,0a2,2 0 1,0 -4,0",
              fillColor: "white",
              fillOpacity: 1,
              scale: 3,
              strokeColor: "#4287f5", // "#28a745",
              strokeWeight: 2,
              strokeOpacity: 1,
              labelOrigin: new google.maps.Point(0, -6)
          }
          break;
        case 'zero-elevation-marker':
          return {
            path: "M-2,0a2,2 0 1,0 4,0a2,2 0 1,0 -4,0",
              fillColor: "white",
              fillOpacity: 1,
              scale: 3,
              strokeColor: "orange", // "#28a745",
              strokeWeight: 2,
              strokeOpacity: 1,
              labelOrigin: new google.maps.Point(0, -6)
          }
          break;

      }
      return;
    }
  }
}