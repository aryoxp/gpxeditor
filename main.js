const {
  app,
  BrowserWindow,
  dialog,
  ipcMain
} = require('electron')
const path = require('path')
const fs = require('fs')

function createWindow() {
  const win = new BrowserWindow({
    width: 1380,
    height: 900,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true
    }
  })
  process.env.GOOGLE_API_KEY = 'AIzaSyCjLU08DWP-wBEfkl4lyGyOiMPV6iiEnoE'
  win.loadFile('main.html')
  if (process.argv[2] == "debug")
    win.webContents.openDevTools()
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

ipcMain.on('open-gpx', (event, arg) => {

  if (arg) { // open gpx from drag and drop
    console.log("Opening GPX file from drag and drop...")
    readAndOpenFile(arg, event)
    return;
  }

  // open gpx from open dialog
  dialog.showOpenDialog({
    properties: ['openFile']
  }).then(result => { // console.log(result.filePaths);
    if (result.canceled) return;
    let filepath = result.filePaths[0];
    readAndOpenFile(filepath, event)
    return;
  });
})

ipcMain.on('save-gpx', (event, arg) => {
  dialog.showSaveDialog({
    filters: [{
      name: 'GPX File',
      extensions: ['gpx']
    }]
  }).then(result => {
    console.log(result);
    if (result === undefined) return;
    if (result.canceled) return;
    let content = '<?xml version="1.0" encoding="UTF-8"?>' +
    '\n<gpx version="1.0" creator="CATEYESync" xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd" xmlns="http://www.topografix.com/GPX/1/0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
    '\n  <trk>' +
    '\n    <trkseg>'
    arg.forEach(p => {
      p.lat = p.lat.toFixed(6)
      p.lng = p.lng.toFixed(6)
      content +=
        '\n      <trkpt lat="' + p.lat + '" lon="' + p.lng + '">' +
        '\n        <ele>' + (p.ele ? p.ele : -10) + '</ele>' +
        '\n        <speed>' + (p.speed ? p.speed : 0) + '</speed>' +
        '\n        <time>' + p.time + '</time>' +
        '\n      </trkpt>';
    });
    content += '\n    </trkseg>' +
    '\n  </trk>' +
    '\n</gpx>'

    fs.writeFile(result.filePath, content, "utf-8", () => {
      dialog.showMessageBoxSync({
        type: "info",
        message: "Data saved successfully."
      })
    })
  });
})


function readAndOpenFile(filepath, event) {
  console.log("Read file: " + filepath)
  fs.readFile(filepath, 'utf-8', (err, data) => {
    if (err) {
      alert("An error ocurred reading the file :" + err.message)
      return
    }
    console.log("Sending file data to renderer...")
    event.sender.send("gpx-loaded", data)
  })
}