$(function () {
  let map;
  let gpxEditor;

  function initMap() {

    map = new G.Map(document.getElementById("map"), {
      center: {
        lat: -34.397,
        lng: 150.644
      },
      zoom: 16,
      clickableIcons: false
    });
  
    gpxEditor = new GPXEditor(map);

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          // if file is not loaded, then center map 
          if(!gpxEditor.pathPolylines.length) 
            map.setCenter(pos);
        },
        (e) => {
          // handleLocationError(true, infoWindow, map.getCenter());
          console.error(e, "Browser doesn't support Geolocation");
        }
      );
    } else {
      console.error("Browser doesn't support Geolocation");
      // Browser doesn't support Geolocation
      // handleLocationError(false, infoWindow, map.getCenter());
    }

  }

  initMap();

})