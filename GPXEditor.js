const {
  app,
  clipboard,
  ipcRenderer,
  remote
} = require('electron')
const {
  Menu,
  MenuItem,
  dialog
} = remote

class GPXEditor {
  constructor(map, options) {

    this.map = map;

    this.data = new GPXData();

    // Timeline
    this.timeline = new vis.Timeline(document.getElementById('visualization'),
      this.data.getDataSet(), Config.Timeline);
    this.timeline.setWindow(Config.Timeline.min, Config.Timeline.max);
    this.timeline.on('select', (e) => this.onTimelineSelect(e));

    // Context Menu
    this.markerContextMenu = CtxMenu.markerInstance(map,
      this.onMarkerContextMenuItemClicked.bind(this))
    this.segmentContextMenu = CtxMenu.segmentInstance(map,
      this.onSegmentContextMenuItemClicked.bind(this))
    this.mapContextMenu = CtxMenu.mapInstance(map,
      this.onMapContextMenuItemClicked.bind(this))

    // Marker Selector
    this.markerSelector = new MarkerSelector(this.map);

    // Info Window
    this.infoWindow = new G.InfoWindow({
      content: "",
      map: map
    });

    this.handleIpcEvent();
    this.handleEvent();
    this.applyConfig();

    // Working pallette
    this.segment = null
    this.seg = null
    this.pathPolylines = [];
    this.clusters = [];

  }

  handleIpcEvent() {
    let app = this;

    // File loaded listener
    ipcRenderer.on('gpx-loaded', (event, arg) => { // console.log(event, arg)  
      this.updateStatus("Loading data...")
      setTimeout(() => {
        console.log("Clearing previous works...")
        this.hideSegments();
        this.clearSeg()
        console.log("GPX data read. Parsing...")
        this.data.parseGpxString(arg);
        console.log("Parse complete. Post-processing " + this.data.length + " points...")
        this.updateSegmentList(this.data.segmentize())
        this.showPath()
        this.fitMap(this.data.pathArray)
        console.log("Show timeline")
        this.showTimeline();
        this.updateStatus("Ready.")
        $.showNotification({
          body: "File loaded.",
          type: "success"
        })
      }, 10)
    })

    // Handle drag and drop listeners.
    document.addEventListener('drop', function (e) { // console.log(e)
      e.preventDefault()
      e.stopPropagation()
      for (let f of e.dataTransfer.files) { // console.log(f)
        ipcRenderer.send('open-gpx', f.path)
      }
    })

    document.addEventListener('dragover', (e) => {
      e.preventDefault();
      e.stopPropagation();
    });
  }

  showTimeline() {
    if (!this.data.length) return;
    this.timeline.setOptions({
      min: this.data.first.time,
      max: (new Date(Date.parse(this.data.last.time) + (5 * 1000))).toISOString()
    })
    this.timeline.setItems(this.data.items)
    setTimeout(() => {
      this.timeline.focus(this.data.first.id, {
        zoom: false
      })
    }, 1000)
  }

  handleEvent() {
    // let app = this;
    this.map.addListener('click', () => {
      if (this.segment) this.segment.clearSelectedMarkers()
      this.clearSeg(true);
      this.clearSelectedItems();
      this.deleteTrailPath();
      // if (app.infoWindow) app.infoWindow.close();
    });

    this.map.addListener('rightclick', (e) => {
      this.rightClickedPosition = {
        lat: e.latLng.lat(),
        lng: e.latLng.lng()
      }
      this.mapContextMenu.show();
    });

    // Show clusters on zoom changed
    this.map.addListener('zoom_changed', (e) => {
      this.showClusters(this.nonWorkingPath ? this.nonWorkingPath : this.data.path)
    })

    // When doing box selection
    this.markerSelector.addListener((type, data) => { // console.log(boundary);
      switch (type) {
        case "bounding-box":
          if (this.segment) {
            this.deleteTrailPath()
            this.segment.markMarkers(new G.LatLngBounds(data.sw, data.ne))
            let selectedIds = this.segment.selectedMarkerIds;
            this.timeline.setSelection(selectedIds)
            if (selectedIds.length) this.timeline.focus(selectedIds[0], {
              zoom: false
            })
          }
          break;
        case "ctrl-s":
          if (this.segment.selectedMarkers.length == 1) {
            let marker = this.segment.selectedMarkers[0]
            let point = marker.point
            let pos = new G.LatLng(data.lat(), data.lng());
            marker.setMap(null)
            marker.setPosition(pos)
            marker.setMap(this.map)
            point.lat = pos.lat()
            point.lng = pos.lng()
            point.ele = undefined
            if (point.next) {
              point = point.next
              let nextMarker = this.segment.getMarkerAt(point.id) // console.log(nextMarker)
              if (nextMarker) {
                this.segment.clearSelectedMarkers()
                this.segment.selectedMarkers.push(nextMarker)
                marker.setIcon(M.icon("zero-elevation-marker"))
                nextMarker.setIcon(M.icon("selected-marker"))
              }
            }
          }
          break;
        case "ctrl-shift-s":
          if (this.segment.selectedMarkers.length == 1) {
            let marker = this.segment.selectedMarkers[0]
            let point = marker.point
            let pos = new G.LatLng(data.lat(), data.lng());
            marker.setMap(null)
            marker.setPosition(pos)
            marker.setMap(this.map)
            point.lat = pos.lat()
            point.lng = pos.lng()
            point.ele = undefined
            if (point.prev) {
              point = point.prev
              let nextMarker = this.segment.getMarkerAt(point.id) // console.log(nextMarker)
              if (nextMarker) {
                this.segment.clearSelectedMarkers()
                this.segment.selectedMarkers.push(nextMarker)
                marker.setIcon(M.icon("zero-elevation"))
                nextMarker.setIcon(M.icon("selected-marker"))
              }
            }
          }
          break;
      }
    })

    $('#bt-load').on('click', (e, callback) => { // console.log(ipcRenderer, dialog)
      ipcRenderer.send('open-gpx')
    })

    $('#bt-show-all-segments').on('click', (e) => {
      if (!this.data.pathArray.length) return;
      this.clearSeg()
      this.hideSegments()
      this.showPath();
      this.fitMap(this.data.pathArray)
      if (typeof callback == "function") callback();
    })

    $('#segments').on('change', (e, callback) => {
      let sid = $("#segments option:selected").data("sid")
      let segment = this.data.segments.get(sid)
      this.showSegment(segment, true)
      if (typeof callback == "function") callback()
    });

    $('#bt-show-segment').on('click', (e, callback) => {
      $('#segments').trigger('change', () => {
        if (typeof callback == "function") callback()
      });
    });

    $('#bt-build-segment').on('click', (e, callback) => {
      this.clearSeg()
      this.hideSegments()
      this.showPath();
      if (typeof callback == "function") callback();
    })

    $('#bt-get-elevation').on('click', () => {
      $('#bt-get-elevation').prop("disabled", true)
      this.updateElevations();
    })

    $('#bt-save').on('click', () => {
      let path = this.data.walk(null, null, true) // console.log(path)
      ipcRenderer.send('save-gpx', path);
    })

    $('#modal-split button.bt-close').on('click', function (event) {
      $('#modal-split').hide();
    });

    $('#modal-split button.bt-split').on('click', (e) => {
      let split = parseInt($('#modal-split input[type="range"]').val()) + 1
      let [a, b] = this.seg.seg;
      let y1 = a.point.lat
      let y2 = b.point.lat
      let x1 = a.point.lng
      let x2 = b.point.lng

      let ydiff = (y2 - y1) / split;
      let xdiff = (x2 - x1) / split;
      let tdiff = Math.round((b.point.utcd / split) * 10) / 10;

      let afterPoint = a.point
      for (let s = 1; s < split; s++) {
        afterPoint = this.data.add(
          a.point.lat + (s * ydiff),
          a.point.lng + (s * xdiff),
          (new Date((a.point.utc + (s * tdiff)) * 1000)).toISOString(),
          afterPoint
        );
      }
      // Update segment internals
      this.segment.update()
      this.showPath(this.segment)
      this.showSegment(this.segment, true)
      $('#modal-split').hide();
    })

    $('#adjust-seconds').on('change', function (event) {
      let seconds = $('#adjust-seconds').val()
      let date = new Date(seconds * 1000).toISOString().substr(11, 8)
      let [h, m, s] = date.split(':')
      let d = Math.floor(seconds / 86400)
      h = parseInt(h)
      m = parseInt(m)
      s = parseInt(s)
      console.log(d, h, m, s)
      $('#adjust-day').val(d)
      $('#adjust-hour').val(h)
      $('#adjust-minute').val(m)
      $('#adjust-second').val(s)
      $('#adjust-day').trigger('change')
      $('#adjust-hour').trigger('change')
      $('#adjust-minute').trigger('change')
      $('#adjust-second').trigger('change')
    })

    $('#adjust-day').on('change', function (event) {
      $('#modal-adjust .days').text($(this).val() + " day")
    })

    $('#adjust-hour').on('change', function (event) {
      $('#modal-adjust .hours').text($(this).val() + " hour")
    })

    $('#adjust-minute').on('change', function (event) {
      $('#modal-adjust .minutes').text($(this).val() + " minute")
    })

    $('#adjust-second').on('change', function (event) {
      $('#modal-adjust .seconds').text($(this).val() + " second")
    })

    $('#modal-adjust .bt-ahead').on('click', (event) => {

      if (!this.trailPath && !this.segment) return
      if (!this.trailPath && this.segment.selectedMarkers.length == 0) return;

      let d = parseInt($('#adjust-day').val())
      let h = parseInt($('#adjust-hour').val())
      let m = parseInt($('#adjust-minute').val())
      let s = parseInt($('#adjust-second').val())

      let ts = (s + (m * 60) + (h * 60 * 60) + (d * 24 * 60 * 60))

      if (this.trailPath) {
        this.trailPath.walkPath.forEach(p => {
          p.utc -= ts
          p.time = (new Date(p.utc * 1000)).toISOString()
          if (p.prev) p.utcd = p.utc - p.prev.utc
          if (p.next) p.next.utcd = p.next.utc - p.utc
          let item = {
            id: p.id,
            content: "",
            start: p.time,
            type: 'point',
            selectable: true
          }
          this.data.items.update(item)
        })
        this.timeline.focus(this.trailPath.walkPath[0].id, {
          zoom: false
        })
      } else {
        this.segment.selectedMarkers.forEach(m => {
          console.log(m)
          m.point.utc -= ts
          if (m.point.prev) m.point.utcd = m.point.utc - m.point.prev.utc
          if (m.point.next) m.point.next.utcd = m.point.next.utc - m.point.utc
          m.point.time = (new Date(m.point.utc * 1000)).toISOString()
          let item = {
            id: m.point.id,
            content: "",
            start: m.point.time,
            type: 'point',
            selectable: true
          }
          this.data.items.update(item)
        })
        this.timeline.focus(this.segment.selectedMarkerIds[0], {
          zoom: false
        })
      }

      this.timeline.setOptions({
        min: this.data.first.time,
        max: (new Date(Date.parse(this.data.last.time) + (5 * 1000))).toISOString()
      })
    })

    $('#modal-adjust .bt-behind').on('click', (event) => {

      if (!this.trailPath && !this.segment) return
      if (!this.trailPath && this.segment.selectedMarkers.length == 0) return;

      let d = parseInt($('#adjust-day').val())
      let h = parseInt($('#adjust-hour').val())
      let m = parseInt($('#adjust-minute').val())
      let s = parseInt($('#adjust-second').val())

      let ts = (s + (m * 60) + (h * 60 * 60) + (d * 24 * 60 * 60))

      if (this.trailPath) {
        this.trailPath.walkPath.forEach(p => {
          p.utc += ts
          p.time = (new Date(p.utc * 1000)).toISOString()
          if (p.prev) p.utcd = p.utc - p.prev.utc
          if (p.next) p.next.utcd = p.next.utc - p.utc
          let item = {
            id: p.id,
            content: "",
            start: p.time,
            type: 'point',
            selectable: true
          }
          this.data.items.update(item)
        })
        this.timeline.focus(this.trailPath.walkPath[0].id, {
          zoom: false
        })
      } else {
        this.segment.selectedMarkers.forEach(m => {
          console.log(m)
          m.point.utc += ts
          if (m.point.prev) m.point.utcd = m.point.utc - m.point.prev.utc
          if (m.point.next) m.point.next.utcd = m.point.next.utc - m.point.utc
          m.point.time = (new Date(m.point.utc * 1000)).toISOString()
          let item = {
            id: m.point.id,
            content: "",
            start: m.point.time,
            type: 'point',
            selectable: true
          }
          this.data.items.update(item)
        })
        this.timeline.focus(this.segment.selectedMarkerIds[0], {
          zoom: false
        })
      }
      this.timeline.setOptions({
        min: this.data.first.time,
        max: (new Date(Date.parse(this.data.last.time) + (5 * 1000))).toISOString()
      })
    })

    $('#modal-adjust button.bt-close').on('click', function (event) {
      $('#modal-adjust').hide();
    });

    $('#ticklength').on("change", (e) => {
      Config.tickLength = parseInt($('#ticklength').val())

      if (this.segment) {
        this.showPath(this.segment)
        this.showSegment(this.segment, true)
      } else this.showPath()
    })

  }

  applyConfig() {
    let ticklength = Config.tickLength
    $('#ticklength option[value="' + ticklength + '"]').prop("selected", true)
  }

  clearSelectedItems() {
    this.timeline.setSelection()
  }

  showClusters(path) { // console.log(path)

    if (!path || path.length == 0) return

    let potentialPoints = []
    let candidate = (type, point, projection) => {
      return {
        type: type,
        lat: point.lat,
        lng: point.lng,
        point: point,
        projection: {
          y: projection.y * (1 << this.map.getZoom()),
          x: projection.x * (1 << this.map.getZoom())
        }
      }
    }

    // for (const [key, p] of path) {
    for (let p of path) {
      let projection = Haversine.projection(p.lat, p.lng);
      if (p.utcd > (Config.tickLength * 2) && p.prev && (p.id == p.prev.next.id)) {
        potentialPoints.push(candidate("long-delay", p, projection))
      } else if (p.ele == undefined || p.ele == -10) {
        potentialPoints.push(candidate("zero-elevation", p, projection))
      }
    }

    let clusterIndex = 0;
    this.clearClusters();
    while (potentialPoints.length) {
      let index = Math.floor(Math.random() * Math.floor(potentialPoints.length - 1));
      let cp = potentialPoints.splice(index, 1)[0]; // console.log(cp)
      let cluster = {
        type: cp.type,
        points: []
      }
      cluster.points.push(cp);

      for (let i = potentialPoints.length - 1; i >= 0; i--) {
        let tp = potentialPoints[i]; // console.log(cp, tp)
        let d = Haversine.pxDistance(cp.projection.x, cp.projection.y, tp.projection.x, tp.projection.y); // console.log(d);
        if (d < 24 && tp.type == cluster.type) {
          cluster.points.push(potentialPoints.splice(i, 1)[0])
        }
      }

      // Get cluster center position
      let n = cluster.points.length;
      let latt = 0,
        lngt = 0
      for (let i = 0; i < n; i++) {
        latt += cluster.points[i].lat
        lngt += cluster.points[i].lng
      }
      cluster.lat = latt / n;
      cluster.lng = lngt / n;

      let type = "cluster-" + cluster.type;
      if (cluster.points.length == 1) {
        let p = cluster.points[0];
        if (p.point.ele == undefined || p.point.ele == -10)
          type = "zero-elevation"
        else if (p.point.utcd > (2 * Config.tickLength))
          type = "long-delay"
        else type = p
      }

      let marker = new G.Marker({
        position: cluster, // it has lat and lng properties
        map: this.map,
        draggable: false,
        icon: M.icon(type),
        label: {
          text: cluster.points.length > 1 ? cluster.points.length.toString() : " ",
          color: type == "cluster-zero-elevation" ? "black" : "white"
        },
        point: cluster.points.length > 1 ? null : cluster.points[0],
        zIndex: 1
      });

      if (type == "cluster-" + cluster.type)
        G.event.addListener(marker, 'click', (e) => this.onClusterClicked(e));
      else G.event.addListener(marker, 'click', (e) => this.onLongMarkerClicked(marker));

      this.clusters.push(marker)
    }
    // console.log(this.clusters)
  }

  clearClusters() {
    this.clusters.forEach(m => {
      m.setMap(null)
      m = null
    })
    this.clusters = [];
  }

  showPath(workingSegment = null) {
    this.clearPath();
    let drawPath = (head, lastId = null) => { // console.log(head, lastId)
      let walk = []
      if (!head || head.id == lastId) return walk;
      let node = head
      do {
        walk.push(node) // console.log(node.id)
        if (node.id == lastId) break;
      } while (node = node.next)
      // let walkValues = walk.values();
      // console.log(Array.from(walkValues))
      let polyline = new G.Polyline({
        path: walk,
        geodesic: true,
        strokeColor: "#28a745",
        strokeOpacity: 1.0,
        strokeWeight: 5,
        map: this.map
      });
      G.event.addListener(polyline, 'click', this.onPathClicked.bind(this))
      this.pathPolylines.push(polyline)
      return walk;
    }
    if (workingSegment) {
      let prePath = drawPath(this.data.first, workingSegment.minId)
      let postPath = drawPath(this.data.path.get(workingSegment.maxId))
      // cache non working path
      this.nonWorkingPath = new Map([...prePath, ...postPath])
      this.showClusters(this.nonWorkingPath);
    } else {
      // clear cache
      if (this.nonWorkingPath) this.nonWorkingPath = []
      this.nonWorkingPath = drawPath(this.data.first)
      this.showClusters(this.nonWorkingPath);
    }
    this.showStartFinishMarkers();
  }

  showStartFinishMarkers() {
    // this.hideStartFinishMarkers()
    if (this.data.length == 0) {
      this.hideStartFinishMarkers()
      return;
    }
    let s = {
      lat: this.data.first.lat,
      lng: this.data.first.lng,
    }
    if (this.startMarker) this.startMarker.setPosition(s)
    else {
      this.startMarker = new G.Marker({
        position: s,
        map: this.map,
        draggable: false,
        icon: M.icon("start"),
        zIndex: 0
      });
    }
    let f = {
      lat: this.data.last.lat,
      lng: this.data.last.lng,
    }
    if (this.finishMarker) this.finishMarker.setPosition(f)
    else {
      this.finishMarker = new G.Marker({
        position: f,
        map: this.map,
        draggable: false,
        icon: M.icon("finish"),
        zIndex: 0
      });
    }

    let min = Infinity
    let max = 0
    let node = this.data.head;
    do {
      if (!node) break;
      let t = Date.parse(node.time)
      if (min > t) min = t
      if (max < t) max = t
    } while (node = node.next)
    if (this.data.size) {
      this.timeline.setOptions({
        min: new Date(min),
        max: (new Date(max + (5 * 1000))).toISOString()
      })
    }

  }

  hideStartFinishMarkers() {
    if (this.finishMarker) this.finishMarker.setMap(null);
    if (this.startMarker) this.startMarker.setMap(null);
  }

  clearPath() {
    this.pathPolylines.forEach(pl => pl.setMap(null))
    this.pathPolylines = []
  }

  fitMap(path) {
    let bounds = new G.LatLngBounds();
    path.forEach(p => bounds.extend(new G.LatLng(p.lat, p.lng)));
    this.map.fitBounds(bounds);
  }

  updateElevations() {

    var done = (success = true) => {
      $('#bt-get-elevation').prop("disabled", false)
      if (success) {
        console.log("Done.")
        if (this.segment) {
          this.showSegment(this.segment, true)
          this.showPath(this.segment)
        } else this.showPath()
        $.showNotification({
          body: "<strong><small>Done.</small><strong>",
          type: "success"
        })
      }
    }

    let process = (p) => {

      // if (p.ele == -10 || p.ele == undefined) {
      console.log("Processing elevation for p: " + p.id, p.lat, p.lng)
      $.ajax({
        method: 'get',
        url: 'http://localhost:8080/api/v1/lookup?locations=' + p.lat + "," + p.lng,
        dataType: "json",
      }).done(result => { // console.log("got: ", result)
        let ele = result.results[0].elevation
        p.ele = (typeof ele == "number") ? ele : 0
        console.log("got: " + ele + " -> " + p.ele)

        let point = p.next
        let found = false;
        while (point) {
          if (point.ele == -10 || point.ele == undefined) {
            found = !found;
            process(point)
            break;
          }
          point = point.next
        }
        if (!found) done()
      }).fail(done(false))
    }

    var point = this.data.first;
    let found = false;
    while (point) {
      if (point.ele == -10 || point.ele == undefined) {
        found = !found;
        process(point)
        break;
      }
      point = point.next
    }
    if (!found) done()
  }

  updateSegmentList(segments) {
    $('#segments').text("");
    segments.forEach(s => {
      $('#segments').append('<option data-sid="' + s.id + '" data-min="' + s.minId + '" data-max="' + s.maxId + '">' + s.minId + " ~ " + s.maxId + '</option>');
      let start = this.data.path.get(s.minId).time
      let end = this.data.path.get(s.maxId).time
      this.data.items.update({
        id: "S" + s.id,
        content: 'Segment #' + s.id,
        start: start,
        end: end,
        type: 'background'
      })
    });
  }

  updateStatus(arg, type = "process") {
    let c = ""
    let s = " &bull; "
    switch (type) {
      case "marker":
        let marker = arg
        let point = marker.point
        let speed = 0
        if (point.prev) {
          let d = Haversine.distance(point, point.prev)
          // console.log(d)
          if (point.utcd) speed = (d * 1000 / point.utcd * 3.6).toFixed(1)
          else speed = "Zero"
        }
        c = "Marker: " + marker.getPosition().lat().toFixed(6) + ", " +
          marker.getPosition().lng().toFixed(6)
        c += s + "Point: " + point.lat.toFixed(6) + ", " + point.lng.toFixed(6)
        c += s + "id: " + point.id
        c += s + "ele: " + point.ele + " m"
        c += s + "speed: " + (point.speed * 3.6).toFixed(1) + " km/h " + ", " +
          speed + " km/h"
        c += s + "time: " + point.time

        break;
      default:
        c = arg
        break;
    }
    $("#status").html("<small class=\"ml-2\">" + c + "</small>")
  }

  onPathClicked(e) {
    let segment = this.data.getSegment(e.latLng.lat(), e.latLng.lng())
    if (segment) {
      this.segment = segment
      this.showSegment(segment)
    }
  }

  onSegmentClicked(e, segment) { // console.error(e)
    let seg = segment.segAt(e.latLng.lat(), e.latLng.lng())
    if (!seg) return;
    this.showSeg(seg);
  }

  showSeg(seg) {
    if (!seg[0] || !seg[1]) return
    let splitable = seg[1].point.utcd >= (2 * Config.tickLength);
    this.clearSeg()
    this.seg = new G.Polyline({
      path: [seg[0].getPosition(), seg[1].getPosition()],
      geodesic: true,
      strokeColor: splitable ? "#6666FF" : "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 5,
      zIndex: 2,
      map: this.map
    });
    this.seg.seg = seg
    this.seg.splitable = splitable
    if (splitable) {
      G.event.addListener(this.seg, 'dblclick',
        (e) => this.onSegDoubleClickListener(e, seg));
      G.event.addListener(this.seg, 'rightclick',
        (e) => this.onSegRightClicked(seg));
    }
  }

  clearSeg(nullify = false) {
    if (this.seg) this.seg.setMap(null);
    if (nullify) this.seg = null
  }

  onSegRightClicked(seg) {
    this.segmentContextMenu.show()
    return;
  }

  onSegDoubleClickListener(e, seg) {
    let pp = seg[0].point
    let np = seg[1].point

    if (np.utcd < (2 * Config.tickLength)) {
      console.warn("Unable to add point: path too close in time distance.")
      e.stop();
      return;
    }

    let pDist = Haversine.calculate(pp.lat, pp.lng, e.latLng.lat(), e.latLng.lng());
    let nDist = Haversine.calculate(np.lat, np.lng, e.latLng.lat(), e.latLng.lng());
    let time = pDist < nDist ?
      new Date((pp.utc + Config.tickLength) * 1000).toISOString() :
      new Date((np.utc - Config.tickLength) * 1000).toISOString()

    let p = this.data.add(e.latLng.lat(), e.latLng.lng(), time, pp)

    // Force segmentize
    // this.updateSegmentList(this.data.segmentize())
    // let segment = this.data.getSegmentWithPoint(p.id)

    // Update segment internals
    this.segment.update()

    // Refresh segment visual
    this.showSegment(this.segment, true)

    e.stop();
  }


  onSegmentContextMenuItemClicked(menuItem, browserWindow, event) {
    let [a, b] = this.seg.seg
    let max = Math.floor(b.point.utcd / Config.tickLength) - 1
    // console.log(a, b, max)
    $('#modal-split input[type="range"]').attr("max", max)
    $('#modal-split small.segments').text(max + " segments")
    $('#modal-split input[type="range"]').val(max)
    $('#modal-split').show();
  }

  onMapContextMenuItemClicked(menuItem, browserWindow, event) {
    switch (menuItem.id) {
      case "add-point":
        if (this.data.size) {
          this.segment = Array.from(this.data.segments.values())[this.data.segments.size - 1]
          let p = this.data.last;
          let time = ((Date.parse(p.time) / 1000) + Config.tickLength) * 1000
          let np = this.data.add(this.rightClickedPosition.lat,
            this.rightClickedPosition.lng, new Date(time).toISOString(), p)
          this.segment.maxId = np.id
          this.segment.update()
        } else {
          this.data.add(this.rightClickedPosition.lat,
            this.rightClickedPosition.lng, new Date().toISOString())
          this.updateSegmentList(this.data.segmentize())
          this.segment = Array.from(this.data.segments.values())[this.data.segments.size - 1]
        }
        this.showSegment(this.segment, true)
        this.updateSegmentList(this.data.segments)
        break;
    }
  }

  onClusterClicked(event) {
    this.map.panTo(event.latLng);
    this.map.setZoom(this.map.getZoom() + 2)
  }

  onLongMarkerClicked(marker) { // console.log(marker)
    let segment = this.data.getSegmentWithPoint(marker.point.point.id)
    this.segment = segment;
    this.showPath(segment)
    this.showSegment(segment);

    let mb = segment.getMarkerAt(marker.point.point.id)
    let ma = segment.getMarkerAt(marker.point.point.prev.id)

    this.showSeg([ma, mb])
  }

  onMarkerClicked(marker) { // console.log(event)
    if (this.segment.selectedMarkers.length == 0 || marker != this.segment.selectedMarkers[0]) {
      marker.setIcon(M.icon("selected-marker"))
      this.segment.clearSelectedMarkers()
      this.segment.selectedMarkers.push(marker)

      if (marker.point.utcd >= (2 * Config.tickLength)) {
        let mb = marker
        let ma = this.segment.getMarkerAt(marker.point.prev.id)
        this.showSeg([ma, mb])
      }
    }

    this.updateStatus(marker, "marker")

    this.timeline.focus(marker.point.id, {
      zoom: false
    });
    this.timeline.setSelection(marker.point.id)
    this.deleteTrailPath()
    $("#segments option[data-sid=\"" + this.segment.id + "\"").prop("selected", true);

    //   let content = "Coordinate: <br>" + point.data.lat + ", " + point.data.lng +
    //     "<br><strong>Data:</strong>" +
    //     "<br>speed: " + (point.data.speed ? point.data.speed.toFixed(1) + " m/s; " + (point.data.speed * 3.6).toFixed(1) + " km/h " :
    //       "-") + "@" + point.data.utcd + "s " +
    //     "<br>elev: " + (point.data.ele ? point.data.ele.toFixed(2) : "-") + " m" +
    //     "<br><strong>Previous:</strong> " + (pvDistance * 1000).toFixed(1) + " m<br>" +
    //     (pvDistance * 1000 / point.data.utcd).toFixed(1) + " m/s ~ " +
    //     (pvDistance * 1000 * 3.6 / point.data.utcd).toFixed(1) + " km/h" +
    //     "<br><strong>Next:</strong> " + (nxDistance * 1000).toFixed(1) + " m<br>" +
    //     (nxDistance * 1000 / nPoint.data.utcd).toFixed(1) + " m/s ~ " +
    //     (nxDistance * 1000 * 3.6 / nPoint.data.utcd).toFixed(1) + " km/h";
  }

  onMarkerRightClicked(marker) { // console.log(event)
    this.markerContextMenu.show()
    this.rightClickedMarker = marker
  }

  onMarkerContextMenuItemClicked(menuItem, browserWindow, event) { // console.log(menuItem, browserWindow, event)
    let app = this;
    let marker = this.rightClickedMarker;
    switch (menuItem.id) {

      case "copy":
        if (this.trailPath) {
          let head = this.trailPath.walkPath[0]
          let tail = this.trailPath.walkPath[this.trailPath.walkPath.length - 1]
          let path = this.data.walk(head.id, tail.id, true)
          clipboard.writeText(JSON.stringify(path));
        } else {
          const {
            head,
            tail
          } = GPXData.findHeadAndTail(this.segment.selectedMarkers)
          let path = this.data.walk(head.id, tail.id, true)
          clipboard.writeText(JSON.stringify(path));
        }
        break;
      case "paste":

        let appendedPath = JSON.parse(clipboard.readText())
        let node = marker.point
        appendedPath.forEach(p => {
          node = this.data.insertAt(node, p.lat, p.lng, p.ele, p.speed, p.time, p.utc, p.id)
        })
        if (marker.point.id == this.segment.maxId)
          this.segment.maxId = node.id
        this.segment.update();
        this.showPath(this.segment)
        this.showSegment(this.segment, true)
        this.updateSegmentList(this.data.segments)
        this.timeline.setOptions({
          min: this.data.first.time,
          max: (new Date(Date.parse(this.data.last.time) + (5 * 1000))).toISOString()
        })
        break;
      case "all-following":
        let walkPath = this.data.walk(marker.point.id)
        this.deleteTrailPath();
        this.trailPath = new G.Polyline({
          path: walkPath,
          geodesic: true,
          strokeColor: "orange",
          strokeOpacity: 1,
          strokeWeight: 6,
          zIndex: 2,
          map: this.map,
        });
        this.trailPath.walkPath = walkPath
        this.timeline.setSelection(GPXData.getIds(walkPath))
        break;
      case "all-following-working":
        let segmentWalkPath = this.data.walk(marker.point.id, this.segment.maxId)
        this.deleteTrailPath();
        this.trailPath = new G.Polyline({
          path: segmentWalkPath,
          geodesic: true,
          strokeColor: "orange",
          strokeOpacity: 1,
          strokeWeight: 6,
          zIndex: 2,
          map: this.map,
        });
        this.trailPath.walkPath = segmentWalkPath
        this.timeline.setSelection(GPXData.getIds(segmentWalkPath))
        break;
      case "adjust-time":
        $('#modal-adjust').show()
        break;
      case "delete":
        dialog.showMessageBox(remote.getCurrentWindow(), {
          message: "DELETE selected points?",
          buttons: ["Yes", "Cancel"]
        }).then((result) => {
          if (result.response === 0) {
            // Delete master points
            // and fix segment's minId and maxId before actual delete
            this.data.delete(this.segment.selectedMarkerIds, (p) => {
              if (p.id == this.segment.minId) {
                this.segment.minId = p.next ?
                  (this.segment.points.has(p.next.id) ? p.next.id : null) :
                  null
              }
              if (p.id == this.segment.maxId) {
                this.segment.maxId = p.prev ?
                  (this.segment.points.has(p.prev.id) ? p.prev.id : null) :
                  null
              }
            })
            // console.log(this.data.path)
            // Delete segment referenced points
            this.segment.delete(this.segment.selectedMarkerIds)
            this.showPath(this.segment)
            this.showSegment(this.segment, true)
            this.updateSegmentList(this.data.segments)
          }
        })
        break;
    }
  }

  deleteTrailPath() {
    if (this.trailPath) {
      this.trailPath.setMap(null);
      this.trailPath = null
    }
  }

  onMarkerDrag(marker) {
    if (!this.segment.selectedMarkers.includes(marker)) {
      marker.setIcon(M.icon("selected-marker"))
      this.segment.clearSelectedMarkers()
      this.segment.selectedMarkers.push(marker)
    }
    if (this.seg && this.seg.seg.includes(marker)) {
      this.showSeg(this.seg.seg)
    }
  }

  onTimelineSelect(event) {
    if (this.segment && this.segment.points.has(event.items[0])) {
      for (const [k, m] of this.segment.markers) {
        if (m.point.id == event.items[0]) {
          this.segment.clearSelectedMarkers()
          this.segment.selectedMarkers.push(m)
          m.setIcon(M.icon("selected-marker"))
          break;
        }
      }
    } else {
      for (const [k, s] of this.data.segments) {
        if (s.points.has(event.items[0])) {
          this.segment = s
          this.showPath(s)
          this.showSegment(s)
          for (const [k, m] of this.segment.markers) {
            if (m.point.id == event.items[0]) {
              this.segment.clearSelectedMarkers()
              this.segment.selectedMarkers.push(m)
              m.setIcon(M.icon("selected-marker"))
              break;
            }
          }
          break;
        }
      }
    }
  }

  showSegment(segment, refresh = false) {

    this.hideSegments();
    if (!refresh && segment.polyline) segment.polyline.setMap(this.map)
    else {
      segment.polyline = new G.Polyline({
        path: segment.path,
        geodesic: true,
        strokeColor: "red",
        strokeOpacity: 0,
        strokeWeight: 3,
        zIndex: 1,
        icons: [{
          icon: Config.lineSymbol,
          offset: "0",
          repeat: "13px",
        }],
        map: this.map
      });
      G.event.addListener(segment.polyline, 'click',
        (e) => this.onSegmentClicked(e, segment))
    }

    // Show path, excluding segment
    this.showPath(segment)

    // Binding Marker
    segment.polyline.binder = new PathBinder(segment.polyline.getPath())

    if (!refresh && segment.markers.size)
      for (const [key, m] of segment.markers)
        m.setMap(this.map)
    else {
      segment.deleteMarkers()
      segment.markers = new Map();
      let segmentMarkerId = 0;
      for (const [key, p] of segment.points) {
        let icon = p.utcd >= (2 * Config.tickLength) ?
          "long-delay-marker" : "unselected-marker"
        icon = p.ele == undefined || p.ele == -10 ? "zero-elevation-marker" : icon
        let marker = new G.Marker({
          map: this.map,
          draggable: true,
          icon: M.icon(icon),
          zIndex: 5
        });
        marker.point = p
        marker.bindTo('position', segment.polyline.binder, (segmentMarkerId).toString());
        G.event.addListener(marker, 'click', (e) => {
          this.onMarkerClicked(marker)
        });
        G.event.addListener(marker, 'rightclick', (e) => {
          this.onMarkerRightClicked(marker)
        });
        G.event.addListener(marker, 'dragend', (e) => { // update point position
          marker.point.lat = parseFloat(marker.getPosition().lat().toFixed(6))
          marker.point.lng = parseFloat(marker.getPosition().lng().toFixed(6))
        });
        G.event.addListener(marker, 'drag', (e) => {
          this.onMarkerDrag(marker)
        });
        segment.markers.set(key, marker);
        segmentMarkerId++;
      }
    }
  }

  hideSegments() {
    for (let s of this.data.segments.values()) {
      if (s.polyline) s.polyline.setMap(null);
      if (s.markers)
        for (const [key, m] of s.markers)
          m.setMap(null);
    }
    this.clearSeg()
  }

}